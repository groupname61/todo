package fr.soget.todo.app.endpoint;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import fr.rest.api.api.TodoApi;
import fr.rest.api.api.dto.SimpleTodoDTO;
import fr.rest.api.api.dto.TodoDTO;
import fr.soget.todo.app.model.Todo;
import fr.soget.todo.app.service.TodoService;

@RestController
public class TodoEndpoint implements TodoApi{

	@Autowired
	private TodoService todoService;

	
	/**
     * {@code POST /todo} : Create a new todo.
     *
     * @param body : the todo to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new
     *         todo, or with status {@code 400 (Bad Request)} if the todo is missing parameters.
     */
	@Override
	public ResponseEntity<TodoDTO> createTodo(@Valid TodoDTO todoDto) {
		TodoDTO todo = todoService.createTodo(todoDto);
		return ResponseEntity.ok().body(todo);
	}
	

	/**
     * {@code DELETE /todo/{id}} : Delete a todo.
     *
     * @param id : the id of the todo to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
	@Override
	public ResponseEntity<Void> deleteTodo(Long id) {
		todoService.deleteTodo(id);
		return ResponseEntity.noContent().build();
	}

	/**
     * {@code GET /todo/{id}} : Get a todo.
     *
     * @param id : the id of the todo to g.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)}, and the todo in the body.
     */
	@Override
	public ResponseEntity<TodoDTO> getTodo(Long id) {
		Todo todo = todoService.getTodoById(id);
		return ResponseEntity.ok().body(todoService.toDTO(todo));
	}

	/**
     * {@code GET /todo} : get a list of todos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of todos in body.
     */
	@Override
	public ResponseEntity<List<SimpleTodoDTO>> getTodos() {
		List<SimpleTodoDTO> todos = todoService.getAllTodo();
		return ResponseEntity.ok().body(todos);
	}

	
	 /**
     * {@code PUT /todo/{id} : Updates an existing todo.
     *
     * @param id : the id of the todo to update
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated
     *         todo, or with status {@code 400 (Bad Request)} if the todo id is not valid, or with
     *         status {@code 500 (Internal Server Error)} if the todo couldn't be updated.
     */
	@Override
	public ResponseEntity<TodoDTO> updateTodo(Long id, @Valid TodoDTO body) {
		TodoDTO todo = todoService.updateTodo(id, body);
		return ResponseEntity.ok().body(todo);
	}

}
