package fr.soget.todo.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.soget.todo.app.model.Todo;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long>{

}
