package fr.soget.todo.app.service;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import fr.rest.api.api.dto.SimpleTodoDTO;
import fr.rest.api.api.dto.TodoDTO;
import fr.soget.todo.app.model.Todo;
import fr.soget.todo.app.repository.TodoRepository;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TodoService {
	
	@Autowired
	private TodoRepository todoRepository;

	/**
     *
     * @param todo : the todo to transform as DTO
     * @return a TodoDTO with the information of the todo 
     */
	public TodoDTO toDTO(Todo todo) {
		TodoDTO dto = new TodoDTO();
		dto.setId(todo.getId());;
		dto.setTitle(todo.getTitle());
		dto.setStatus(todo.isStatus());
		dto.setDescription(todo.getDescription());
		return dto;
	}
	
	/**
    *
    * @param todo : the todo to transform as DTO
    * @return a SimpleTodoDTO with the information of the todo 
    */
	private SimpleTodoDTO toSimpleDTO(Todo todo) {
		SimpleTodoDTO simpleDto = new SimpleTodoDTO();
		simpleDto.setId(todo.getId());;
		simpleDto.setTitle(todo.getTitle());
		simpleDto.setStatus(todo.isStatus());
		return simpleDto;
	}
	
	/**
    *
    * @param id : the id of the todo to find
    * @return the Todo with the corresponding id, or an error if the id doesn't exist
    */
	public Todo getTodoById(Long id) {
		return todoRepository.findById(id)
				.orElseThrow(() -> {
					 final String reason = String.format("Todo %d not found", id);
					log.error(reason);
					return new ResponseStatusException(HttpStatus.BAD_REQUEST, reason);
				});
	}

	/**
    *
    * @return the all todo(s) in the DataBase
    */
	public List<SimpleTodoDTO> getAllTodo() {
		// TODO add PageRequest
		List<Todo> todos = todoRepository.findAll();
		List<SimpleTodoDTO> todoDTOs = todos.stream()
				.sorted(Comparator.comparing(Todo::isStatus).thenComparing(Todo::getCreationDate))
				.map(todo -> toSimpleDTO(todo))
				.collect(Collectors.toList());
		return todoDTOs;
	}

	/**
    * @param id : the id of the todo to update
    * @param todo : the new information of the todo
    *  update the todo, whith the id, with the new informations 
    */
	public TodoDTO updateTodo(Long id, @Valid TodoDTO body) {
		Todo todo = getTodoById(id);
		todo.setTitle(body.getTitle());
		todo.setStatus(body.isStatus());
		todo.setDescription(body.getDescription());
		todo.setLastUpdateDate(Instant.now().atOffset(ZoneOffset.UTC));
		
		Todo res = todoRepository.save(todo);
		return toDTO(res);
	}

	/**
	    * @param id : the id of the todo to delete
	    *  delete the todo todo whith the id
	    */
	public void deleteTodo(Long id) {
		Todo todo = getTodoById(id);
		todoRepository.delete(todo);
	}

	/**
	    * @param todo : the information of the todo
	    *  create a new todo with the informations 
	    */
	public TodoDTO createTodo(@Valid TodoDTO body) {
		Todo todo = new Todo();
		todo.setTitle(body.getTitle());
		
		todo.setStatus(body.isStatus());
		if(body.getDescription() != null) {
			todo.setDescription(body.getDescription());
		}
		todo.setCreationDate(Instant.now().atOffset(ZoneOffset.UTC));
		todo.setLastUpdateDate(Instant.now().atOffset(ZoneOffset.UTC));
		
		Todo res = todoRepository.save(todo);
		return toDTO(res);
	}
	
	
}
