package fr.soget.todo.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import fr.soget.todo.app.Application;
import fr.soget.todo.app.model.Todo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class EmployeeControllerIntegrationTest {
	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void contextLoads() {

	}

	@Test
	public void testGetAllTodos() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/todo",
				HttpMethod.GET, entity, String.class);
		
		assertNotNull(response.getBody());
	}

	@Test
	public void testGetTodoById() {
		Todo todo = restTemplate.getForObject(getRootUrl() + "/todo/1", Todo.class);
		assertNotNull(todo);
	}

	@Test
	public void testCreateTodo() {
		Todo todo = new Todo();
		todo.setTitle("my First TODO");
		todo.setStatus(false);
		todo.setDescription("Things TO DO");

		ResponseEntity<Todo> postResponse = restTemplate.postForEntity(getRootUrl() + "/todo", todo, Todo.class);
		assertNotNull(postResponse);
		assertNotNull(postResponse.getBody());
	}

	@Test
	public void testUpdateTodo() {
		int id = 1;
		Todo todo = restTemplate.getForObject(getRootUrl() + "/todo/" + id, Todo.class);
		todo.setTitle("title");
		todo.setStatus(false);

		restTemplate.put(getRootUrl() + "/todo/" + id, todo);

		Todo updatedEmployee = restTemplate.getForObject(getRootUrl() + "/todo/" + id, Todo.class);
		assertNotNull(updatedEmployee);
	}

	@Test
	public void testDeleteTodo() {
		int id = 2;
		Todo todo = restTemplate.getForObject(getRootUrl() + "/todo/" + id, Todo.class);
		assertNotNull(todo);

		restTemplate.delete(getRootUrl() + "/todo/" + id);

		try {
			todo = restTemplate.getForObject(getRootUrl() + "/todo/" + id, Todo.class);
		} catch (final HttpClientErrorException e) {
			assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}
}
