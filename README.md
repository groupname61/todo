<div align="center">
  <br>
  <br>
  <h1>TODO app</h1>
  <p>
      Todo
  </p>
</div>

<br>
<br>

<h2>Table of Contents</h2>

- [Prerequisites](#prerequisites)
- [Setup](#setup)
  - [HowToLaunchBack](#howtolaunchback)
  - [Prepare to run (if change branch or modify swagger)](#prepare-to-run-if-change-branch-or-modify-swagger)
  - [run backend :](#run-backend-)
  - [HowToLaunchOpenAPIDoc](#howtolaunchopenapidoc)


<br>
<hr>
<br>

## Prerequisites

You *must* have installed on your system, the following software:

- Git v2.x.x, latest up-to-date version is recommended (`git --version`): [https://git-scm.com/downloads](https://git-scm.com/downloads)


<br>
<br>

## Setup

1. Clone this repository and submodules via GIT and SSH.
   *(If you want to clone it via HTTP, you must switch submodules URIs to HTTP upon clone)*
2. Download and install Oracle OpenJDK 11: 
      [https://openjdk.java.net/install/](https://openjdk.java.net/install/)
3. Install Eclipse 2020-03 (4.15.0) or newer.
4. Configure Eclipse with the Oracle OpenJDK 11.
5. Add plugin Eclipse: STS 4 (Spring Tools Suite).
6. Launch target Maven build clean install on the pom parent webxml-supply.
7. Install Agent lombok for Eclipse:
      [https://projectlombok.org/setup/eclipse](https://projectlombok.org/setup/eclipse)
8. Download and install PostgreSQL (12) or newer.
9. Create new DataBase "todos".

### HowToLaunchBack
### Prepare to run (if change branch or modify swagger)
- **1** - In eclipse, launch Maven Generate Sources
- **2** - In eclipse, if it is the first launch in eclipse on the directory : **todo\target\generated-sources\swagger\src\gen\java**
right click, Build Path -> use as source folder

### run backend : 

In Eclipse, launch a target Spring Boot App on the project
The server run on http://localhost:8080/

<br>
<br>